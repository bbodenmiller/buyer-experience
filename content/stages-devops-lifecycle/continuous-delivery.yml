---
  title: Continuous Delivery
  description: Learn more about GitLab enablement stage in the DevOps lifecycle.
  components:
    - name: sdl-cta
      data:
        title: Continuous Delivery
        aos_animation: fade-down
        aos_duration: 500
        subtitle: Streamline and Automate your application release process to make software delivery repeatable and on-demand.
        text: |
          A crucial part of the GitLab DevOps platform, GitLab Continuous Delivery makes software delivery repeatable and on-demand, simplifying the hardest part of DevOps while making it flexible, secure and achievable. GitLab Continuous Delivery performs all the steps to deploy your code to your production environment including provisioning infrastructure, managing changes via version control, ticketing and release versioning, progressively deploying code, verifying and monitoring those changes and providing the ability to roll back when necessary - all from the same application that also hosts your source code and helps with Continuous Integration.
        icon:
          name: continuous-delivery
          alt: Continuous Delivery Spotlight Icon
          variant: marketing
    - name: featured-media
      data:
        header: Why GitLab for Continuous Delivery?
        header_animation: fade-up
        header_animation_duration: 500
        column_size: 6
        media:
          - title: Safe Deployments
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              You deploy new versions for customer benefits - more speed, more stability, more features and so on. Having a deployment fail defeats the entire purpose of deploying new versions. Hence GitLab provides deployment strategies to safeguard your deployments - including what to deploy to production (via [Feature Flags](https://docs.gitlab.com/ee/operations/feature_flags.html) ), who to deploy it to (via [Progressive Delivery](https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html)) and advanced deployment strategies like [Canary](https://docs.gitlab.com/ee/user/project/canary_deployments.html) and [blue green deployments](https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html#blue-green-deployment) ).
          - title: Flexible Deployment Options
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              GitLab [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) simplifies and accelerates delivery with a complete delivery pipeline out of the box. Use predefined [deployment templates](https://docs.gitlab.com/ee/ci/examples/index.html) or build and maintain your [pipelines-as-code](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html). Your deployments can also scale with your DevOps maturity - create [deployments that span multi-projects](https://docs.gitlab.com/ee/ci/pipelines/multi_project_pipelines.html) or organize your pipelines with [parent-child pipelines](https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html).
          - title: Integrated GitOps
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Modern infrastructure needs to be elastic. And the sooner you can move away from click-ops and script-ops, the sooner you can achieve this elasticity at scale. With a native integration with Terraform for [infrastructure as code](https://docs.gitlab.com/ee/user/infrastructure/iac/) and a [secure connection with your Kubernetes Clusters](https://docs.gitlab.com/ee/user/clusters/agent/) to detect and recover from drift, GitLab can help you establish an end to end GitOps workflow.
          - title: DORA metrics
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Track your team's / organization's speed of delivery by tracking key [Continuous Delivery related metrics](https://docs.gitlab.com/ee/api/dora/metrics.html) like Deployment Frequency (i.e., how often code is deployed to customers) and Lead time to change (i.e., time taken from code commit to production deployment).
          - title: Multicloud deployment
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              GitLab supports application deployment anywhere, including virtual machines, Kubernetes clusters, or FaaS offerings from different cloud vendors. Whether you use Amazon Web Services, Google Cloud Platform, Microsoft Azure, or your own private cloud - GitLab is infrastructure-agnostic DevOps that is built for [multicloud](https://about.gitlab.com/topics/multicloud/). Check out the supported application deployment targets [here](https://about.gitlab.com/stages-devops-lifecycle/deploy-targets/)
          - title: Continuous compliance
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              GitLab simplifies compliance for continuous delivery by allowing customers to maintain granular policies such as
              - defining who can deploy (via segregation of duties and approval rules)
              - maintain governance and audit trail of all changes
              - govern releases with version history, code and evidence
          - title: Continuous feedback
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: You cannot fix what you cannot see. With GitLab, you can continuously visualize the status of your [environments and deployments](https://docs.gitlab.com/ee/ci/environments/#view-environments-and-deployments) (past, current and upcoming). You can also measure how your deployment is performing (via browser [performance testing](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html), [performance monitoring](https://docs.gitlab.com/ee/operations/metrics/) and [tracing](https://docs.gitlab.com/ee/operations/tracing.html)), [rollback deployment based on performance](https://docs.gitlab.com/ee/ci/environments/#retrying-and-rolling-back) (via post deployment monitoring), and achieve a feedback loop (via [incident management](https://docs.gitlab.com/ee/operations/incident_management/) ).
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/enablement/){data-ga-name="enablement direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Create
            icon:
              name: create-alt-2
              alt: Create Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 500
            text: Create, view, manage code and project data through powerful branching tools.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/create/
              data_ga_name: create
              data_ga_location: body
          - title: Plan
            icon:
              name: plan-alt-2
              alt: Plan Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1000
            text: Regardless of your process, GitLab provides powerful planning tools to keep everyone synchronized.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/plan/
              data_ga_name: plan
              data_ga_location: body
          - title: Manage
            icon:
              name: manage-alt-2
              alt: Manage Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1500
            text: Gain visibility and insight into how your business is performing.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/manage/
              data_ga_name: manage
              data_ga_location: body
