---
  title: What is version control?
  description: Version control software is used to track revisions, solve integration conflicts in code, and manage different artifacts involved in software projects.
  components:
    - name: topics-breadcrumb
    - name: old-topics-header
      data:
        title: What is version control?
        block:
          - metadata:
              id_tag: what-is-version-control
            text: |
              Version control — also known as source control or revision control — is an important software development practice for tracking and managing changes made to code and other files. It is closely related to source code management.
            link_text: Learn how to streamline development
            link_href: https://learn.gitlab.com/scm?utm_content=topicpage&utm_campaign=vccusecase
            data_ga_name: Learn how to streamline developmentt
            data_ga_location: header
            resources:
              topic:
                header: More on this topic
                links:
                  - text: Version control best practices
                    link: /topics/version-control/version-control-best-practices/
                    data_ga_location: header
                    data_ga_name: Version control best practices
                  - text: What is Git version control?
                    link: /topics/version-control/what-is-git-version-control/
                    data_ga_location: header
                    data_ga_name: What is Git version control?
                  - text: What is Git workflow?
                    link: /topics/version-control/what-is-git-workflow/
                    data_ga_location: header
                    data_ga_name: What is Git workflow?
                  - text: What is InnerSource?
                    link: /topics/version-control/what-is-innersource/
                    data_ga_location: header
                    data_ga_name: What is InnerSource?
                  - text: How to implement version control
                    link: /topics/version-control/how-implement-version-control/
                    data_ga_location: header
                    data_ga_name: How to implement version control
    - name: copy
      data:
        block:
          - header: The basics of version control
            hide_horizontal_rule: true
            column_size: 8
            text: |
              With version control, every change made to the code is tracked. This allows developers to see the entire history of who changed what at any given time — and roll back to an earlier version if they need to. It also creates a single source of truth.

              Version control (or source control or revision control) serves as a safety net to protect the source code from irreparable harm, giving the development team the freedom to experiment without fear of causing damage or creating code conflicts.

              If developers code concurrently and create incompatible changes, version control identifies the problem areas so that team members can quickly revert changes to a previous version, compare changes, or identify who committed the problem code through the revision history. With a version control system (VCS), a software team can solve an issue before progressing further into a project. Through code reviews, software teams can analyze earlier versions to understand the changes made to the code over time.

              Depending on a team's specific needs and development process, a VCS can be local, centralized, or distributed. A local VCS stores source files within a local system, a centralized VCS stores changes in a single server, and a distributed VCS involves cloning a Git repository.

              [Learn five ways to enhance team collaboration with version control best practices →](/resources/ebook-version-control-best-practices/){data-ga-name="VC best practices"}{data-ga-location="body"}
    - name: slp-blockquote
      data:
        column_size: 8
        text: |
          **Version control** enables teams to collaborate and streamline development to resolve conflicts and create a centralized location for code.
    - name: copy
      data:
        block:
          - header: Why use version control?
            text: |
              As organizations accelerate delivery of their software solutions through DevOps, controlling and managing different versions of application artifacts — from code to configuration and from design to deployment — becomes increasingly difficult.

              Version control software facilitates coordination, sharing, and collaboration across the entire software development team. It enables teams to work in distributed and asynchronous environments, manage changes and versions of code and artifacts, and resolve merge conflicts and related anomalies.

              [Read how Git Partial Clone lets you fetch only the large files you need →](/blog/2020/03/13/partial-clone-for-massive-repositories/){data-ga-name="Partial clone"}{data-ga-location="body"}
            image:
              image_url: /nuxt-images/version-control/version-control.svg
              alt:
            column: true
            column_size: 8
            inverted: true
            hide_horizontal_rule: true
    - name: copy
      data:
        block:
          - header: What is a version control system?
            text: |
              A version control system (VCS) tracks changes to a file or set of files over time. The most common type is a centralized VCS, which uses a server to store all the versions of a file. Developers can check out a file from the server, make changes, and check the file back in. The server then stores the new version of the file.
            hide_horizontal_rule: true
            column_size: 8
    - name: copy
      data:
        block:
          - header: Types of version control systems
            text: |
              The two most popular types are centralized and distributed. Centralized version control systems store all the files in a central repository, while distributed version control systems store files across multiple repositories. Other less common types include lock-based and optimistic.

              ### Distributed

              A distributed version control system (DVCS) allows users to access a repository from multiple locations. DVCSs are often used by developers who need to work on projects from multiple computers or who need to collaborate with other developers remotely.

              ### Centralized

              A centralized version control system (CVCS) is a type of VCS where all users are working with the same central repository. This central repository can be located on a server or on a developer's local machine. Centralized version control systems are typically used in software development projects where a team of developers needs to share code and track changes.

              ### Lock-based

              A lock-based version control system uses file locking to manage concurrent access to files and resources. File locking prevents two or more users from making conflicting changes to the same file or resource.

              ### Optimistic

              In an optimistic version control system, every user has their own private workspace. When they want to share their changes with the rest of the team, they submit a request to the server. The server then looks at all the changes and determines which ones can be safely merged together.
            hide_horizontal_rule: true
            column_size: 8
    - name: old-benefits-icons
      data:
        title: Benefits of version control
        column_size: 8
        benefits:
          - title: Quality
            icon:
              name: computer-test
              variant: marketing
              alt: Computer Test Icon
            subtitle: |
              Teams can review, comment, and improve each other’s code and assets.
          - title: Acceleration
            icon:
              name: pillar-speed
              variant: marketing
              alt: Pillar Speed Icon
            subtitle: |
              Branch code, make changes, and merge commits faster.
          - title: Visibility
            icon:
              name: first-look-influence
              variant: marketing
              alt: First Look Influence Icon
            subtitle: |
              Understand and spark team collaboration to foster greater release build and release patterns.
    - name: old-benefits-icons
      data:
        title: What are the main version control systems?
        subtitle: The three most well-known version control tools are Git, Subversion, and Mercurial.
        column_size: 8
        benefits:
          - title: Git
            icon:
              name: git
              variant: marketing
              alt: Git Icon
            subtitle: |
              Git is the most popular option and has become synonymous with "source code management." Git is an open source distributed system that is used for software projects of any size, making it a popular option for startups, enterprise, and everything in between.
          - title: Subversion
            icon:
              name: svn
              variant: marketing
              alt: SVN Icon
            subtitle: |
              SVN is a widely adopted centralized VCS. This system keeps all of a project's files on a single codeline making it impossible to branch, so it's easy to scale for large projects. It's simple to learn and features folder security measures, so access to subfolders can be restricted.
          - title: Mercurial
            icon:
              name: mercurial
              variant: marketing
              alt: Mercurial Icon
            subtitle: |
              Mercurial is a distributed VCS that offers simple branching and merging capabilities. The system enables rapid scaling and collaborative development, with an intuitive interface. The flexible command line interface enables users to begin using the system immediately.

    - name: copy
      data:
        block:
          - header: How does version control streamline collaboration?
            text: |
              Version control (or source control) coordinates all changes in a software project, effectively tracking changes to source files, designs, and all digital assets required for a project and related metadata. Without it, projects can easily devolve into a tangled mess of different versions of project files, hindering the ability of any software development team to deliver value.

              With a strong VCS, software teams can quickly assemble all critical project files and foster actionable communication to improve code quality. And because it provides a single source of truth, stakeholders from across a DevOps team can collaborate to build innovative solutions — from product managers and designers to developers and operations professionals.

              [Discover 15 best practices for large teams to innovate and collaborate using source code management →](https://page.gitlab.com/resources-ebook-scm-for-enterprise.html){data-ga-name="SCM for enterprise"}{data-ga-location="body"}
            column_size: 8
    - name: copy-resources
      data:
        title: Next steps in version control
        block:
          - text: |
              Ready to learn more about version control? Here are a few resources to help you get started on your journey.

              [Learn how GitLab streamlines software development →](/solutions/benefits-of-using-version-control/){data-ga-name="Benefits of version control"}{data-ga-location="body"}
            resources:
              webcast:
                header: Webcasts
                links:
                  - text: Learn how to collaborate without boundaries to unlock faster delivery with GitLab
                    link: /webcast/collaboration-without-boundaries/
                    data_ga_name: Learn how to collaborate without boundaries to unlock faster delivery with GitLab
                    data_ga_location: body
              video:
                header: Videos
                links:
                  - text: Watch how GitLab SCM and code review spark velocity
                    link: https://page.gitlab.com/resources-demo-scm.html
                    data_ga_name: Watch how GitLab SCM and code review spark velocity
                    data_ga_location: body
                  - text: Discover how code review and source code management streamline collaboration
                    link: https://page.gitlab.com/resources-demo-scm.html
                    data_ga_name: Discover how code review and source code management streamline collaboration
                    data_ga_location: body
              article:
                header: Articles
                links:
                  - text: Read how version control and collaboration builds a strong DevOps foundation
                    link: /solutions/benefits-of-using-version-control/
                    data_ga_name: Read how version control and collaboration builds a strong DevOps foundation
                    data_ga_location: body
              whitepaper:
                header: Whitepapers
                links:
                  - text: Learn how to move to Git
                    link: /resources/whitepaper-moving-to-git/
                    data_ga_name: Learn how to move to Git
                    data_ga_location: body
              book:
                header: Books
                links:
                  - text: Discover a Git branching strategy to simplify software development
                    link: /resources/ebook-git-branching-strategies/
                    data_ga_name: Discover a Git branching strategy to simplify software development
                    data_ga_location: body
                  - text: Version control best practices eBook to accelerate delivery
                    link: /resources/ebook-version-control-best-practices/
                    data_ga_name: Version control best practices eBook to accelerate delivery
                    data_ga_location: body
              case_study:
                header: Case Studies
                links:
                  - text: Learn how Worldline uses GitLab to improve code reviews
                    link: /customers/worldline/
                    data_ga_name: Learn how Worldline uses GitLab to improve code reviews
                    data_ga_location: body
                  - text: Learn how Cook County assesses economic data with transparency and version control
                    link: /customers/cook-county/
                    data_ga_name: Learn how Cook County assesses economic data with transparency and version control
                    data_ga_location: body
                  - text: Read how Remote uses GitLab to meet 100% of deadlines
                    link: /customers/remote/
                    data_ga_name: Read how Remote uses GitLab to meet 100% of deadlines
                    data_ga_location: body
                  - text: Read how Dublin City University uses GitLab SCM and CI to achieve top results
                    link: /customers/dublin-city-university/
                    data_ga_name: Read how Dublin City University uses GitLab SCM and CI to achieve top results
                    data_ga_location: body
    - name: featured-media
      data:
        header: Suggested Content
        column_size: 4
        media:
          - title: 15 Git tips to improve your workflow
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Learn how to compare commits, delete stale branches, and write aliases to save you some time. It's time to dust off your command line and Git busy!
            link:
              text: Learn more
              href: /blog/2020/04/07/15-git-tips-improve-workflow/
              data_ga_name: 15 Git tips to improve your workflow
              data_ga_location: features
            image:
              url: /nuxt-images/blogimages/git-15th-anniversary-cover.png
          - title: Why you should move from centralized version control to distributed version control
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              We share a few reasons why high-pefrorming software development teams use distributed version control systems over centralized version control.
            link:
              text: Learn more
              href: /blog/2020/11/19/move-to-distributed-vcs/
              data_ga_name: Why you should move from centralized version control to distributed version control
              data_ga_location: features
            image:
              url: /nuxt-images/blogimages/distributedvcs.jpg
          - title: The problem with Git flow
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              Learn why Git flow complicates the lifecycle and discover an alternative to streamline development
            link:
              text: Learn more
              href: /blog/2020/03/05/what-is-gitlab-flow/
              data_ga_name: The problem with Git flow
              data_ga_location: features
            image:
              url: /nuxt-images/blogimages/whatisgitlabflow.jpg
