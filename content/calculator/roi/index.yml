---
  title: Return on Investment Calculator
  description: Here you can find information on the GitLab Categories ROI Calculator. View more here and see the value!
  tooltips:
    numberOfUsers: How many employees are using your current toolset?
    numberOfMaintainers: |
      How many employees install, integrate and manage your toolset? An average annual salary of USD $97,000 is assumed (source: [glassdoor](https://www.glassdoor.co.in/Salaries/us-software-developer-salary-SRCH_IL.0,2_IN1_KO3,21.htm))
    sourceCodeManagement: Annual spend (in USD) for source code management tools including asset version control, branching, and code reviews. (E.g., tools like GitHub, BitBucket)
    continuousIntegration: Annual spend (in USD) for continuous integration tools including build, test, and validation. (E.g., tools like Jenkins, CircleCI)
    continuousDelivery: Annual spend (in USD) for continuous delivery tools used for safe deployments to staging/production environments. (E.g., tools like Harness, ArgoCD)
    applicationSecurity: Annual spend (in USD) for application security tools including static and dynamic testing, amongst others. (E.g., tools like Snyk, Veracode, or Sonarcube)
    agileProjectManagement: Annual spend (in USD) for agile project management tools used for planning within a team/project (E.g., tools like Jira, Trello, or Asana)
    agilePortfolioManagement: Annual spend (in USD) for agile portfolio management tools used for planning across multiple teams/projects. (E.g., tools like Planview Enterprise One, Jira Align)
    registries: Annual spend (in USD) for container and package registries. (E.g., tools like Artifactory, Docker Hub)
  currentSpendTooltip: Current spend includes the costs of tools and maintainers
  premium_features:
    header: 'GitLab Premium includes:'
    button:
      text: Learn more about Premium
      url: '/pricing/premium/'
      data_ga_location: premium roi results
      data_ga_name: learn more about premium
    list:
      - text: Faster code reviews
        information:
          - text: Multiple approvers in code review
          - text: Code Owners
          - text: Code Review Analytics
          - text: Merge request reviews
          - text: Code Quality Reports
          - text: Merged results pipelines
          - text: Comments in Review Apps
      - text: Advanced CI/CD
        information:
          - text: CI/CD Pipelines Dashboard
          - text: Merge Trains
          - text: Multi-project pipeline graphs
          - text: CI/CD for external repo
          - text: GitOps deployment management
          - text: Environments Dashboard
          - text: Group file templates
          - text: Robust deploy and rollback bundle
      - text: Enterprise agile planning
        information:
          - text: Roadmaps
          - text: Multiple Issue Assignees
          - text: Single level Epics
          - text: Issue Board Milestone Lists
          - text: Scoped Labels
          - text: Promote Issue to Epic
          - text: Multiple Group Issue Board
          - text: Issue Dependencies
      - text: Release controls
        information:
          - text: Approval rules for code review
          - text: Required Merge Request Approvals
          - text: Merge Request Dependencies
          - text: Push rules
          - text: Restrict push and merge access
          - text: Protected Environments
          - text: Lock project membership to group
          - text: Geolocation-aware DNS
      - text: Self-managed reliability
        information:
          - text: Disaster Recovery
          - text: Maintenance mode
          - text: Distributed cloning with GitLab Geo
          - text: Fault-tolerant Git storage with Gitaly
          - text: Support for Scaled Architectures
          - text: Container Registry geographic replication
          - text: Database load balancing for PostgreSQL
          - text: Log forwarding
      - text: 10,000 CI/CD minutes per month
      - text: Support
  ultimate_features:
    header: 'GitLab Ultimate includes:'
    button:
      text: Learn more about Ultimate
      url: '/pricing/ultimate/'
      data_ga_location: premium roi results
      data_ga_name: learn more about ultimate
    list:
      - text: Advanced security testing
        information:
          - text: Dynamic Application Security Testing
          - text: Security Dashboards
          - text: Vulnerability Management
          - text: Container Scanning
          - text: Dependency Scanning
          - text: Vulnerability Reports
          - text: API Fuzz Testing
          - text: Vulnerability Database
      - text: Vulnerability management
        information:
          - text: Security Approvals
          - text: Security Policies
      - text: Compliance pipelines
        information:
          - text: Audit events report
          - text: Interface for audit events
          - text: License Compliance
          - text: Compliance report
          - text: Quality Management
          - text: Requirements Management
          - text: Require a Jira issue before merging code
          - text: Custom compliance frameworks
      - text: Portfolio management
        information:
          - text: Epic Boards
          - text: Multi-level Epics
          - text: Issue and Epic Health Reporting
          - text: Planning hierarchy
          - text: Portfolio-level Roadmaps
          - text: Bulk Edit Epics
      - text: Value stream management
        information:
          - text: Insights
          - text: DORA-4 metric - Deployment frequency
          - text: Dora-4 metric - Lead time for changes
      - text: 50,000 CI/CD minutes per month
      - text: Support
      - text: Free guest users
  cards:
  - icon:
      name: money
      alt: Money Icon
      variant: marketing
    title: Learn more about GitLab pricing plans
    link_text: Go to pricing page
    link_url: /pricing
    data_ga_name: learn more about gitlab pricing plans
    data_ga_location: roi calculator
  - icon:
      name: cloud-thin
      alt: Cloud Thin Icon
      variant: marketing
    title: Purchase GitLab through Cloud Marketplaces
    link_text: Contact sales
    link_url: /sales
    data_ga_name: purchase gitlab through cloud marketplaces
    data_ga_location: roi calculator
