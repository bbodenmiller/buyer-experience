---
title: Try GitLab for free
description: Enjoy GitLab Ultimate 30 day free trial and experience the full software development lifecycle & DevOps tool with an extensive range of innovative features.
components:
  - name: call-to-action
    data:
      title: Try GitLab Ultimate for free for 30 days
      centered_by_default: true
      subtitle: Free trial includes nearly all [Ultimate-tier](/pricing/ultimate/){data-ga-name="Free trial includes nearly all Ultimate-tier" data-ga-location="header"} features. No credit card required [[1]](/pricing/faq/#why-do-i-need-to-enter-credit-debit-card-details-for-free-pipeline-minutes){class="cta__subtitle--subscript" data-ga-name="credit card faq" data-ga-location="header"}
      body_text: Start your 30-day trial now. After that, enjoy GitLab Free (forever!).
      aos_animation: fade-down
      aos_duration: 500
  - name: free-trial-plans
    data:
      saas:
        tooltip_text: |
          Software as a service is a software licensing and delivery model in which software is licensed on a subscription basis and is centrally hosted.
        text: |
          We host. No technical setup required. Get started right away – no installation required.
        link:
          text: Continue with SaaS
          href: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=free-trial
          data_ga_name: continue with saas
          data_ga_location: body
        bottom_text: |
          Already have an account? [Log In](https://gitlab.com/users/sign_in?redirect_to_referer=yes&__cf_chl_jschl_tk__=db2d336ba94805d0675008cc3fa3e0882d90953c-1619131501-0-AeQCSleOFTDGa9C-lXa3ZZZPpsO6sh0lCBCPZT0GxdT7tyOMAZoPzKppSQq9eV2Gqq9_kwKB8Lt8GJQ-nF-ra8updJRDfWTMBAwCR-m38kaHdAJYTicvW8Tj4KH55GO25zOeCYJexeEp1hx6f3DMvtjZd8elp_RfdulgN4-rxW8-lFSumJdSzE8y8N9FGltpsoQ8SKFSq41jMoB_GJ1nkIrjCU_kaGxJA3l4xhh-C14XFoBoBtfGjGOH4Kj76Y5QAeT7qemwuGBlvpYCK0OBv5aPkFDZ_Knp0W1zaOkr5tt511fra-rE3ekQI_lwR5VqBTHLtNslfgt4Il1SKLi6ZJLkces_WsUWdIQ3jNlyKbv08CF6kyDI3NiEOcCXUopCfQDYr-5syEUhv1Cnxy-Vjn7u5ejR2pvwIytWm8io2rhcaSOYxzxWccpxZLfjotTkzlrNP7KALbkxQOcNa_zeWVQ5t6aGC8H5wrT8u8ICxuJC){data-ga-name="log in" data-ga-location="body"}
      self_managed:
        form_id: 2150
        form_header: You host. Download and install GitLab on your own infrastructure or in a public cloud environment. Requires Linux experience.
  - name: faq
    data:
      aos_animation: fade-up
      aos_duration: 500
      header: GitLab Trial Frequently Asked Questions (FAQs)
      groups:
        - header: What’s included in a free trial
          questions:
            - question: What is included in my free trial? What’s excluded?
              answer: |
                Your free trial includes nearly all features of our [Ultimate tier](/pricing){data-ga-name="ultimate tier" data-ga-location="faq"}; however, free trials exclude support at any level. If you wish to evaluate GitLab for support expertise or SLA performance specifically, please [contact sales](/sales/){data-ga-name="sales" data-ga-location="faq"} to discuss options.
            - question: What happens after my free trial ends?
              answer: |
                Your GitLab Ultimate trial lasts 30 days. After this period, you can maintain a GitLab Free account forever or upgrade to a [paid plan](/pricing/){data-ga-name="paid plan" data-ga-location="faq"}.
        - header: SaaS vs. Self-Managed
          questions:
            - question: What is the difference between SaaS and Self-Managed setups?
              answer: |
                SaaS: We host. No technical setup required, so you don’t have to worry about downloading and installing GitLab yourself. Self-Managed: You host. Download and install GitLab on your own infrastructure or in our public cloud environment. Requires Linux experience.
            - question: Are certain features included only in SaaS or Self-Managed?
              answer: |
                Certain features are only available on Self-Managed. Compare the [full list of features here](/pricing/feature-comparison/){data-ga-name="features list" data-ga-location="faq"}.
        - header: Pricing and discounts
          questions:
            - question: Is a credit/debit card required for a free trial?
              answer: |
                A credit/debit card is not required for customers who do not use GitLab.com CI/CD, bring their own runners, or disable shared runners. However, credit/debit card details are required if you choose to use GitLab.com shared runners. This change was made to discourage abuse of the free pipeline minutes provided on GitLab.com to mine cryptocurrencies - which created performance issues for GitLab.com users. When you provide the card, it will be verified with a one-dollar authorization transaction. No charge will be made and no money will transfer. Learn more [here](/blog/2021/05/17/prevent-crypto-mining-abuse/){data-ga-name="prevent crypto mining abuse" data-ga-location="faq"}.
            - question: How much does a GitLab license cost?
              answer: |
                Subscription information is listed on our [pricing page](/pricing/){data-ga-name="pricing page" data-ga-location="faq"}.
            - question: Do you have special pricing for open source projects, startups, or educational institutions?
              answer: |
                Yes! We provide free Ultimate licenses to qualifying open source projects, startups, and educational institutions. Find out more by visiting our [GitLab for Open Source](/solutions/open-source/){data-ga-name="gitlab for open source" data-ga-location="faq"}, [GitLab for Startups](/solutions/startups/){data-ga-name="gitlab for startups" data-ga-location="faq"}, and [GitLab for Education](/solutions/education/){data-ga-name="gitlab for education" data-ga-location="faq"} program pages.
            - question: How do I upgrade from GitLab Free to one of the paid subscriptions?
              answer: |
                If you want to upgrade from GitLab Free to one of the paid tiers, follow the [guides in our documentation](https://docs.gitlab.com/ee/update/README.html#community-to-enterprise-edition){data-ga-name="community to enterprise" data-ga-location="faq"}.
        - header: Installation and migration
          questions:
            - question: How do I migrate to GitLab from another Git tool?
              answer: |
                See all the project migration instructions for popular version control systems in [our documentation](https://docs.gitlab.com/ee/user/project/import/index.html){data-ga-name="migration" data-ga-location="faq"}.
            - question: How do I install GitLab using a container?
              answer: |
                Find out about installing GitLab using Docker in [our documentation](https://docs.gitlab.com/omnibus/docker/README.html){data-ga-name="install docker" data-ga-location="faq"}.
        - header: GitLab integrations
          questions:
            - question: Is GitHost still available?
              answer: |
                No, we are no longer accepting new customers for GitHost. More information is available in the [GitHost FAQ](/githost/){data-ga-name="githost faq" data-ga-location="faq"}.
            - question: What tools does GitLab integrate with?
              answer: |
                GitLab offers a number of third-party integrations. Learn more about available services and how to integrate them in [our documentation](https://docs.gitlab.com/ee/integration/README.html){data-ga-name="third party integration" data-ga-location="faq"}.

