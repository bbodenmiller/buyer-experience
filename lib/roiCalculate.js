// CONSTANTS
const avgDevSalary = 97000; // grabbed from calculations sheet
const premium = 228; // $19 per month
const ultimate = 1188; // $99 per month

function checkIfUltimate(
  agilePortfolioManagementSpend,
  applicationSecuritySpend,
) {
  return agilePortfolioManagementSpend > 0 || applicationSecuritySpend > 0;
}

function addUserSpend(
  sourceCodeManagementSpend,
  continuousIntegrationSpend,
  continuousDeliverySpend,
  registriesSpend,
  agileProjectManagementSpend,
  agilePortfolioManagementSpend,
  applicationSecuritySpend,
) {
  // Wrapping each value in a Number function prevents mishaps from an empty number field adding an empty string to this function
  return (
    Number(sourceCodeManagementSpend) +
    Number(continuousIntegrationSpend) +
    Number(continuousDeliverySpend) +
    Number(registriesSpend) +
    Number(agileProjectManagementSpend) +
    Number(agilePortfolioManagementSpend) +
    Number(applicationSecuritySpend)
  );
}

function totalUserSpend(userSpendSum, numberOfMaintainers) {
  return userSpendSum + numberOfMaintainers * avgDevSalary;
}

function totalGitlabSpend(isUltimate, numberOfUsers, gitlabMaintainers) {
  if (isUltimate) {
    return ultimate * numberOfUsers + avgDevSalary * gitlabMaintainers;
  }
  return premium * numberOfUsers + avgDevSalary * gitlabMaintainers;
}

module.exports = {
  checkIfUltimate,
  addUserSpend,
  totalUserSpend,
  totalGitlabSpend,
};
